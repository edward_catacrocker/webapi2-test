﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Dto
{
    public class RoleDto
    {
        public int Id { get; set; }
        // public int IdRole { get; set; }
        public string RoleName { get; set; }
    }
}
